exports.config = {
    onPrepare: function() {
        var SpecReporter = require('jasmine-spec-reporter');
        jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: 'all'}));
    },
    baseUrl:'https://angularjs.org/',
    framework:'jasmine2',
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    seleniumAddress: 'http://ec2-50-112-128-166.us-west-2.compute.amazonaws.com:4444/wd/hub',
    capabilities: {
        'browserName': 'chrome'
    },
    specs: ['../tests/integration/*.js'],
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000,
        stackTrace: false,
        print: function() {}

    }
};