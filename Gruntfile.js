module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-protractor-runner');
    grunt.loadNpmTasks('grunt-protractor-webdriver');
    grunt.loadNpmTasks('grunt-jasmine-nodejs');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            // if any .less file changes in directory 'css/less/' run the 'less'-task.
            files: 'css/less/*.less',
            tasks: ['less', 'notify:lessMsg']
        },
        // 'less'-task configuration
        less: {
            development: {
                options: {
                    // Specifies directories to scan for @import directives when parsing.
                    paths: ['css/less/']
                },
                files: {
                    // compilation.css  :  source.less
                    'css/app.css': 'css/app.less'
                }
            }
        },
        notify: {
            lessMsg: {
                options: {
                    title: 'Less',
                    message: 'Less compilation succesfully finished!'
                }
            }
        },
        jshint: {
            all: ['Gruntfile.js', 'js/modules/**/*.js', 'js/components/**/*.js', 'js/helpers/**/*.js', 'js/services/*.js'],
            options: {
                jshintrc: '.jshintrc'
            }
        },

        protractor: {
            options: {
                configFile: 'tests/conf.js',
                keepAlive: true,
                noColor: false,
                args: {
                    // Arguments passed to the command
                }
            },
            e2e: {
                options: {
                    keepAlive: true
                }
            }
        },
        //start the selenium server before executing integration tests
        protractor_webdriver: {
            start: {
                options: {
                    command: 'webdriver-manager start'
                }
            }
        },
        jasmine_nodejs: {

            options: {
                specNameSuffix: 'spec.js',
                helperNameSuffix: 'helper.js',
                useHelpers: false,
                random: false,
                seed: null,
                defaultTimeout: null,
                stopOnFailure: false,
                traceFatal: true,
                reporters: {
                    console: {
                        colors: true,
                        cleanStack: 1,
                        verbosity: 4,
                        listStyle: 'indent',
                        activity: false
                    }
                },

                customReporters: []
            },
            unit: {

                options: {
                    useHelpers: true
                },
                specs: [
                    'tests/unit/**'
                ]

            }
        }
    });

    // Default task(s).
    grunt.registerTask('default', ['less', 'watch']);
    grunt.registerTask('e2e-tests', ['protractor_webdriver:start', 'protractor:e2e']);
    grunt.registerTask('unit-tests', ['jasmine_nodejs:unit']);

};
