angular
    .module('component-action-menu')
    .directive('actionmenu', actionMenu);

function actionMenu(){
    return {
        restrict: 'AE',
        scope: {
            items: '='
        },
        templateUrl : 'js/components/action-menu/action-menu.html',
        link: link
    };
}

function link(scope){
    scope.menuItems = [];
    scope.$watch('items', function(data){
        scope.menuItems = data;
    });
}