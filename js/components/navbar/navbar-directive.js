angular
    .module('component-navbar')
    .directive('navbar', navbar);

function navbar(){
    return {
        restrict: 'AE',
        templateUrl : 'js/components/navbar/navbar.html'
    };
}