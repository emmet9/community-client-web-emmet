angular
    .module('hashtagify',[])
    .directive('hashtagify', ['$timeout', '$compile',
        function($timeout, $compile) {
            return {
                restrict: 'A',
                scope: {
                    uClick: '&userClick',
                    tClick: '&termClick'
                },
                link: function(scope, element, attrs) {
                    $timeout(function() {
                        var html = element.html();

                        if (html === '') {
                            return false;
                        }
                        html = html.replace(/(|\s)*@(\w+)/g, '$1<a class="hashtag">@$2</a>');

                        html = html.replace(/(^|\s)*#(\w+)/g, '$1<a class="hashtag">#$2</a>');


                        element.html(html);

                        $compile(element.contents())(scope);
                    }, 10);
                }
            };
        }
    ]);
