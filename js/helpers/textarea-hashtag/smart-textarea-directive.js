angular
    .module('areahashtag',[])
    .directive('smartTextArea', smartTextArea);

function smartTextArea() {
    return {
        restrict: 'A',
        scope: {
            words: '='
        },
        link: function(scope, element) {
            element.hashtags();
            

            scope.$watch('words', function(newVal){
                if(newVal){
                    startAutocomplete(newVal);
                }
            });

            function startAutocomplete(words){
                element.textcomplete([
                    {
                        words: words,
                        match: /([@|#][A-Za-z0-9_\-]+)$/,
                        search: function (term, callback) {
                            callback($.map(this.words, function (word) {
                                return word.toLowerCase().indexOf(term.toLowerCase()) === 0 ? word : null;
                            }));
                        },
                        index: 1,
                        replace: function (word) {
                            return word + ' ';
                        }
                    }
                ], {
                    onKeydown: function (e, commands) {
                        if (e.ctrlKey && e.keyCode === 74) {
                            return commands.KEY_ENTER;
                        }
                    }
                });
            }

        }
    };
}