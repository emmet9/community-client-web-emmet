/*global angular*/
'use strict';
angular
    .module('community')
    .controller('homeCtrl',
        function () {
            var homeModel = this;
            var questions = [];

            homeModel.questions = [];
            homeModel.page = 1;
            homeModel.per_page = 10;
            homeModel.sort = '-dt_create';
            homeModel.actionMenuList= {
                questions: {
                    display: true,
                    active: true,
                    link: 'home',
                    text: 'Questions'
                },
                tags: {
                    display: true,
                    active: false,
                    link: 'tags',
                    text: 'Tags'
                },
                users: {
                    display: true,
                    active: false,
                    link: 'users',
                    text: 'Users'
                },
                ask: {
                    display: true,
                    active: false,
                    special: true,
                    link: 'ask',
                    text: 'Ask a Question'
                }
            };

            /* Loads the questions given a sort parameter */
            homeModel.loadQuestions = function (params) {

                //mocked up stamplay service response
                questions[0] = {
                    answers:13,
                    authorNickName:"GraceF",
                    _id:"5761307e82ec5d610ae900a9",
                    tags: 42,
                    text: "How do you do keyword research for Germany, Italy, France & Spain? I'm totally confused on how i should pick the correct keywords for the different marketplaces. I mean, i don't…",
                    title:"How to do keyword research for Germany ?",
                    views: 42
                };
                questions[1] = {
                    answers:2,
                    authorNickName:"KevinG",
                    _id:"57612a1282ec5d610ae9005b",
                    tags: 42,
                    text: "Is it normal that my product research takes a long time ? i spent hours and hours picking my 20 product list. I followed the system step by step. I don't know if what i did was really time…",
                    title:"What is the best way to research a product to sell?",
                    views: 42
                };
                questions[2] = {
                    answers:10,
                    authorNickName:"JennyB",
                    _id:"576129fe82ec5d610ae9005a",
                    tags: 11,
                    text: "Hello I'm working through lesson-name, and I'm really struggling with finding suppliers for my products. Has anyone else experienced this? I don't know if my keywords are off, or if perhaps there is something wrong in my sea...",
                    title:"What to do if my country of residence is not suported to sell on Amazon.co.uk ?",
                    views: 22
                };
                questions[3] = {
                    answers:9,
                    authorNickName:"JennyB",
                    _id:"577111d3a516cdc01b309fc2",
                    tags: 22,
                    text: "Hello I'm working through lesson-name, and I'm really struggling with finding suppliers for my products. Has anyone else experienced this? I don't know if my keywords are off, or if perhaps there is something wrong in my sea...",
                    title: "How did you find a Supplier for your Product?",
                    views: 17
                };

                homeModel.questions = questions;
                homeModel.total = result.pagination;
                homeModel.page++;
                homeModel.busy = false;
            };

            homeModel.loadNext = function (page) {
                if (homeModel.total === homeModel.questions.length) return;

                homeModel.busy = true;
                var params = {
                    sort: homeModel.sort,
                    page: page,
                    per_page: homeModel.per_page,
                    populate_owner: true,
                    populate: true
                }

                homeModel.loadQuestions(params);


            };

            /* Listener on tab */
            homeModel.sortQuestion = function (sortOn) {
                homeModel.busy = true;
                homeModel.page = 1;
                homeModel.totalLength = 0;
                homeModel.questions.length = 0;
                switch (sortOn) {
                    case 'newest':
                        homeModel.sort = '-dt_create';
                        break;
                    case 'votes':
                        homeModel.sort = '-actions.votes.total';
                        break;
                    case 'active':
                        homeModel.sort = '-dt_update';
                        break;
                    case 'trending':
                        homeModel.sort = '-dt_trending';
                        break;
                    default:
                        homeModel.sort = '-dt_create';
                        break;
                }

                homeModel.loadQuestions({
                    sort: homeModel.sort,
                    page: homeModel.page,
                    per_page: homeModel.per_page,
                    populate_owner: true,
                    populate: true
                });
            };
        }
    );