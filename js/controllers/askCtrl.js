/*global angular*/
'use strict';

angular
	.module('community')
	.controller('askCtrl', ['$rootScope', '$http', '$location',
		function ($rootScope, $http, $location) {
			var askModel = this;
			askModel.actionMenuList= {
				questions: {
					display: true,
					active: false,
					link: 'home',
					text: 'Questions'
				},
				tags: {
					display: true,
					active: false,
					link: 'tags',
					text: 'Tags'
				},
				users: {
					display: true,
					active: false,
					link: 'users',
					text: 'Users'
				},
				ask: {
					display: true,
					active: true,
					special: false,
					link: 'ask',
					text: 'Ask a Question'
				}
			};
		}
	]);