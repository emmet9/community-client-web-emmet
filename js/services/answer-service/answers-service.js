angular
    .module('communityAPI')
    .service('answersService', answersService);

answersService.$inject = ['$http'];

function answersService($http) {

    return{
        getAnswer : getAnswer
    };

    function getAnswer(){
        return $http.get('/js/services/answer-service/answer.json')
    }
}