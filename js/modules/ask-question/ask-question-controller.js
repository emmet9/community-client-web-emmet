/*global angular*/

angular
    .module('ask-question')
    .controller('askCtrl', ['$uibModal', '$rootScope',
        function ($uibModal, $rootScope) {
            'use strict';
            
            var askModel = this;
            askModel.actionMenuList= {
                questions: {
                    display: true,
                    active: false,
                    link: 'home',
                    text: 'Questions'
                },
                tags: {
                    display: true,
                    active: false,
                    link: 'tags',
                    text: 'Tags'
                },
                users: {
                    display: true,
                    active: false,
                    link: 'users',
                    text: 'Users'
                },
                ask: {
                    display: true,
                    active: true,
                    special: false,
                    link: 'ask',
                    text: 'Ask a Question'
                }
            };


            askModel.open = function () {
                $rootScope.hasOpenedVideo = true;
                $uibModal.open({
                    templateUrl: '/js/helpers/video-modal/video.html'
                });
            };

            if(!$rootScope.hasOpenedVideo){
                mixpanel.track('Video play', {'Sponsored': true, 'Video Location': 'Home feed'});
                askModel.open();
            }
        }
    ]);