/*global angular*/
angular.module('community', [
	'infinite-scroll',
	'ngRoute', 
	'ui.router', 
	'ui.bootstrap', 
	'textAngular',
	'answers',
	'communityAPI',
	'component-navbar',
	'component-action-menu',
    'component-footer',
    'hashtagify',
    'ask-question',
	'youtube-embed',
	'notification'
]);

angular
	.module('community')
	.config(function ($stateProvider, $urlRouterProvider) {
		'use strict';

		$urlRouterProvider.otherwise('/');

		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: '/pages/home.html',
				controller: 'homeCtrl',
				controllerAs: 'home'
			})
			.state('homev2', {
				url: '/homev2',
				templateUrl: '/pages/homev2.html',
				controller: 'homeCtrl',
				controllerAs: 'home'
			})
			.state('homev3', {
				url: '/homev3',
				templateUrl: '/pages/homev3.html',
				controller: 'homeCtrl',
				controllerAs: 'home'
			})
			.state('search-results', {
				url: '/search-results',
				templateUrl: '/pages/search-results.html',
				controller: 'homeCtrl',
				controllerAs: 'home'
			})
			.state('ask', {
				url: '/questions/ask',
				templateUrl: '/js/modules/ask-question/ask-question.html',
				controller: 'askCtrl',
				controllerAs: 'askModel'
			})
			.state('answer', {
				url: '/questions/:id',
				templateUrl: '/js/modules/answers/answer.html',
				controller: 'answersCtrl'
			});
	});