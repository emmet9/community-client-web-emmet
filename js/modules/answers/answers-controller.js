/*global angular*/

angular
    .module('answers')
    .controller('answersCtrl', Answers);

Answers.$inject = ['$scope','answersService', 'tagsService', '$rootScope'];

function Answers($scope, answersService, tagsService, $rootScope) {
    'use strict';
    
    $scope.voteUp = voteUp;
    $scope.voteDown = voteDown;
    $scope.postAnswer = postAnswer;
    $scope.likeQuestion = likeQuestion;
    $scope.showAnswerBox = showAnswerBox;
    
    $scope.answerMsg = '';
    $scope.hasLikedQuestion = false;
    $scope.hasVoteUp = false;
    $scope.hasVoteDown = false;

    $scope.actionMenuList= {
        questions: {
            display: true,
            active: true,
            link: 'home',
            text: 'Questions'
        },
        tags: {
            display: true,
            active: false,
            link: 'tags',
            text: 'Tags'
        },
        users: {
            display: true,
            active: false,
            link: 'users',
            text: 'Users'
        },
        ask: {
            display: true,
            active: false,
            special: true,
            link: 'ask',
            text: 'Ask a Question'
        }
    };

    ////////////////////////////////////
    
    answersService
        .getAnswer()
        .success(function(data){
            $scope.question = data.question;
        });

    tagsService
        .getTags()
        .success(function(data){
            $scope.suggestedWords = data.tags;
        });

    /**
     * Vote up an answer, this takes original answer
     * and finds index position of that answer in the original array.
     * The indexes are changed with Angular order By. Unlock
     * voting down, lock voting up.
     * @param answer
     */
    function voteUp(answer){
        var originalIndex = $scope.question.answersList.indexOf(answer);
        $scope.question.answersList[originalIndex].vote += 1;
        $scope.hasVoteDown = false;
        $scope.hasVoteUp = true;

        $rootScope.$broadcast('notification:add', {msg:'Voted Up!', duration: '3000'});
    }

    /**
     * Vote down an answer, this takes original answer
     * and finds index position of that answer in the original array.
     * The indexes are changed with Angular order By. Unlock voting up
     * lock voting down
     * @param answer
     */
    function voteDown(answer){
        var originalIndex = $scope.question.answersList.indexOf(answer);
        $scope.question.answersList[originalIndex].vote -= 1;
        $scope.hasVoteDown = true;
        $scope.hasVoteUp = false;
        $rootScope.$broadcast('notification:add', {msg:'Voted Down!', duration: '3000'});
    }

    function postAnswer(){
        var tmp = {
            title: 'Test 3',
            authorNickName: 'Sharon',
            vote: 0,
            comments: 0,
            msg: 'Test',
            _id:'577111d3a516cdc01b309fc2'
        };
        tmp.msg = $scope.answerMsg;
        $scope.question.answersList.push(tmp);
        $scope.expandAnswerBox = false;
    }

    /**
     * Like question and disable the button
     */
    function likeQuestion(){
        $scope.question.votes += 1;
        $scope.hasLikedQuestion = true;
    }
    
    function showAnswerBox(){
        $scope.expandAnswerBox = true;
    }

}